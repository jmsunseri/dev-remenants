import type { ISize } from "./ISize";

export interface IFolder {
  path: string;
  selected: boolean;
  size: ISize;
  ignore: string;
}

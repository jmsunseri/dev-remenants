export interface IPlatform {
    name: string,
    folders: string[],
}
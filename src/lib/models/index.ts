import type { IFolder } from './IFolder';
import type { IPlatform } from './IPlatform';
import type { ISize } from './ISize';

const platforms : IPlatform[] = [{
    name: 'dotnet',
    folders: ['obj', 'bin', '.nuget/packages']
},{
    name: 'node',
    folders: ['node_modules']
}, {
    name: 'rust',
    folders: ['target']
},{
    name: 'go',
    folders: ['vendor']
},
{
    name: 'java',
    folders: ['bin', 'target']
}


]



export { IFolder, IPlatform, platforms, ISize };

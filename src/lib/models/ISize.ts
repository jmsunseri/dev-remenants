export interface ISize {
    bytes: number;
    mb: number;
}
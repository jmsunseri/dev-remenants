import { writable } from 'svelte/store';
import type { IFolder } from './models';

export const folders = writable<IFolder[]>([]);
export const directory = writable<string>();
export const gitIgnoreCount = writable<number>();

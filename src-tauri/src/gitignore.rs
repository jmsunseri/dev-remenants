
use std::{path::{Path, PathBuf}, fs::{DirEntry}};

use ignore::gitignore::Gitignore;

pub trait ToGitIgnore {
    fn to_git_ignore(&self) -> Option<Gitignore>;
}

pub trait IsGitIgnore {
    fn is_git_ignore(&self) -> bool;
}

pub trait IsGitIgnored {
    fn is_ignored(&self, git_ignore_files: &Vec<&Gitignore>) -> Option<String>;
}

impl IsGitIgnore for DirEntry {
    fn is_git_ignore(&self) -> bool {
        let file_type = self.file_type();
        match file_type {
            Ok(ft) => {
                if ft.is_file() {
                    let file_name = self.file_name();
                    if file_name.to_ascii_lowercase() == ".gitignore" {
                        return true
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            },
            _ => false,
        }
    }
}

impl IsGitIgnored for DirEntry {
    fn is_ignored(&self, git_ignore_files: &Vec<&Gitignore>) -> Option<String> {
        for f in git_ignore_files.iter() {
            if f.matched(&self.path(), true).is_ignore() {
                return Some(f.path().display().to_string())
            } 
        }
        None
    }
}

impl ToGitIgnore for Path {
    fn to_git_ignore(&self) -> Option<Gitignore> {
        let result = Gitignore::new(self);
        Some(result.0)
    }
}

impl ToGitIgnore for PathBuf {
    fn to_git_ignore(&self) -> Option<Gitignore> {
        self.as_path().to_git_ignore()
    }
}

impl ToGitIgnore for Option<PathBuf> {
    fn to_git_ignore(&self) -> Option<Gitignore> {
        match self {
            Some(s) => s.as_path().to_git_ignore(),
            _ => None
        }
    }
}

pub struct DirectoryPaths {
    pub paths: Vec<PathBuf>,
}

impl DirectoryPaths {
    pub fn git_ignore_files(&self) -> Vec<Gitignore> {
        self.paths
            .iter()
            .filter_map(|p| p.to_git_ignore())
            .collect()
    }

    pub fn from(entries: &Vec<DirEntry>) -> DirectoryPaths {
        DirectoryPaths { 
            paths: entries.iter()
            .filter(|d| d.is_git_ignore())
            .map(|r| r.path()).collect()
        }
    }
}

use tauri::Window;


use crate::{GitIgnoreEvent, common::Folder};


pub trait Emitters {
    fn gitignore_event(&self, message: &str, gi_path: &str);
    fn gitignored_folder_event(&self, folder: &Folder);
}

impl Emitters for Window {
    fn gitignore_event(&self, message: &str, gi_path: &str) {
        match self.emit("gitignore-event", GitIgnoreEvent { 
            path: String::from(gi_path),
            message: String::from(message)
        }) 
        {
            Err(_) => println!("error emitting event to window"),
            _ => {}
        }
    }
    
    fn gitignored_folder_event(&self, folder: &Folder) {
        match self.emit("gitignore-folder-event", folder) 
        {
            Err(_) => println!("error emitting event to window"),
            _ => {}
        }
    }
}



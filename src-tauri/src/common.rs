use std::{path::PathBuf, fs::DirEntry};

use byte_unit::{Byte, ByteUnit};
use fs_extra::dir::get_size;

#[derive(serde::Serialize, serde::Deserialize, Clone)]
pub struct Folder {
    pub path: String,
    pub selected: bool,
    pub size: Size,
    pub ignore: String,
}

#[derive(serde::Serialize, serde::Deserialize, Clone)]
pub struct Size {
    pub bytes: u64,
    pub mb: f64,
}

impl Size {
    pub fn from(entry: &DirEntry) -> Size {
        let bytes = calculate_size(&entry.path());
        Size {
            bytes,
            mb: calculate_mb(bytes)
        }
    }
}

fn calculate_mb(bytes: u64) -> f64 {
    let result = Byte::from_bytes(bytes as u128);
    let mb = result.get_adjusted_unit(ByteUnit::MB);
    mb.get_value()
}

fn calculate_size(path: &PathBuf) -> u64 {
    match get_size(path) {
        Ok(bytes) => bytes,
        _ => 0
    }
}
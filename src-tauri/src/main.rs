// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

fn main() {
    tauri::Builder::default()
        .invoke_handler(tauri::generate_handler![
            get_dev_remenants,
            delete_remenants
        ])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}

use std::{
    fs::{self, DirEntry},
};

#[derive(Clone, serde::Serialize)]
struct GitIgnoreEvent {
  message: String,
  path: String,
}

use async_recursion::async_recursion;

use ignore::gitignore::Gitignore;
use tauri::Window;
mod gitignore;
mod event_emitters;
mod common;

use gitignore::{
    DirectoryPaths,
    IsGitIgnored
};

use event_emitters::{
    Emitters
};

use common::{
    Folder
};

use crate::common::Size;




#[derive(serde::Serialize, serde::Deserialize)]
struct Platform {
    name: String,
    folders: Vec<String>,
}



// Learn more about Tauri commands at https://tauri.app/v1/guides/features/command
#[tauri::command]
async fn get_dev_remenants(window: Window, directory: String) -> Vec<Folder> {
    let mut folders: Vec<Folder> = vec![];
    let git_ignore_files: Vec<&Gitignore> = vec![];
    read_folder(&*directory, &mut folders, &git_ignore_files, &window).await;
    folders.sort_by(|a, b| b.size.bytes.cmp(&a.size.bytes));
    folders
}

#[tauri::command]
async fn delete_remenants(folders: Vec<Folder>) {
    for folder in folders {
        println!("Deleting file: {}", folder.path);

        let result = fs::remove_dir_all(folder.path);

        match result {
            Ok(_) => println!("File successfully deleted"),
            Err(_) => println!("There was an error deleting the file."),
        }
    }
}



async fn read_folder(path: &str, folders: &mut Vec<Folder>, git_ignore_files: &Vec<&Gitignore>, window: &Window) {
    let read_dir = fs::read_dir(path);

    match read_dir {
        Ok(read_dir) => {
            println!("reading directory: {}", path);
            let entries: Vec<DirEntry> = read_dir.filter_map(|s| s.ok()).collect();
            let directory_paths = DirectoryPaths::from(&entries);

            let new_files = directory_paths.git_ignore_files();
            let mut new_files: Vec<&Gitignore> = new_files.iter().collect();

            if new_files.len() > 0 {
                match directory_paths.paths[0].to_str() {
                    Some(path) => {
                        window.gitignore_event("file found" , path);
                        println!("found new git ignore files {}", path);
                    }
                    _ => {}
                }
            }

            if new_files.len() == 0 {
                process_directory_entries(
                    entries, 
                    folders, 
                    &git_ignore_files,
                    window).await
            } else if git_ignore_files.len() == 0 {
                process_directory_entries(
                    entries, 
                    folders, 
                    &new_files, 
                    window).await
            } else {
                new_files.extend(git_ignore_files.into_iter());
                process_directory_entries(
                    entries, 
                    folders, 
                    &new_files,
                    window
                ).await;
            }         
        },
        Err(e) => println!(
            "There was an error reading the current directory: {} {}",
            path, e
        ),
    }
}


#[async_recursion]
async fn process_directory_entries(
    entries: Vec<DirEntry>,
    folders: &mut Vec<Folder>,
    git_ignore_files: &Vec<&Gitignore>,
    window: &Window
) {
    println!("processing entries... ignore files: {}, entries: {}", git_ignore_files.len(), entries.len());
    for entry in entries {
        let file_type = entry.file_type();
        println!("processing entry: {}", entry.path().display());
        match file_type {
            Ok(x) => {
                if x.is_dir()
                    && !x.is_symlink()
                {
                    match entry.is_ignored(git_ignore_files) {
                        Some(ignore_path) => {
                            let size = Size::from(&entry);
                            if size.mb >= 1.0 {
                                let folder = Folder {
                                    path: entry.path().display().to_string(),
                                    selected: false,
                                    size,
                                    ignore: ignore_path
                                };

                                window.gitignored_folder_event(&folder);
                                folders.push(folder);
                                
                                println!("ignored folder!  {}", entry.path().display().to_string());
                            }
                        },
                        _ => {
                            read_folder(
                                &*entry.path().display().to_string(),
                                folders,
                                git_ignore_files,
                                window
                            )
                            .await
                        }
                    }
                } 
            }
            Err(_) => println!(
                "There was an error reading file file type of {}",
                entry.path().display()
            ),
        }
    }
}





